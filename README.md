# NodeJs Shop REST CMS
## Install:
```bash
  $ npm i -g babel-cli
  $ npm i -g nodemon
  $ npm i
```

And create custom-data.txt which include this data, add this file to .gitignore
```bash
  npm run init-data
```

### Run Server in prod:
```bash
  $ npm start
```

### Run Server in dev:
```bash
  $ npm run dev
```