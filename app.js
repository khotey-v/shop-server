import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import morgan from 'morgan';
import cors from 'cors';
import bluebird from 'bluebird';
import mongoose from 'mongoose';

import config from './app/config';

const app = express();
const env = app.get('env');
const { database } = config;

mongoose.Promise = bluebird;
mongoose.connect(database, err => {
  if (err) throw err;

  if (env === 'dev') {
    console.log('MongoDB connected');
  }
});

if (env === 'dev') {
  app.use(morgan('tiny'));
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

export default app;
