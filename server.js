import app from './app/main';
import config from './app/config';

const { port } = config;

app.listen(port, err => {
  if (err) throw err;

  console.log(`Server listening port ${port}`);
});
