import * as UserService from '../services/user-service';

export default async function (req, res, next) {
  const { token } = req;

  try {
    var user = await UserService.getUserByToken(token);
  } catch ({ message }) {
    return next({
      status: 500,
      message
    });
  }

  if (!user) {
    return next({
      status: 403,
      message: 'Permission denied'
    });
  }

  req.user = user;
  next();
}
