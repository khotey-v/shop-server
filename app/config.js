const config = {
  port: 3000,
  database: 'localhost/makeup',
  secret: 'khotey'
};

const env = process.env.NODE_ENV;

if (env === 'test') {
  config.database += '-test';
}

export default config;
