import express from 'express';

import * as PageController from '../controllers/page-controller';
import checkToken from '../middlewares/checkToken';
import getUser from '../middlewares/getUser';

const router = express.Router();

router.get('/pages', PageController.getAll);
router.get('/pages/:url', PageController.getByUrl);
router.post('/pages', checkToken, getUser, PageController.create);
router.put('/pages', checkToken, getUser, PageController.update);
router.delete('/pages', checkToken, getUser, PageController.deletePage);

export default router;