import express from 'express';

import * as PagesStatusController from '../controllers/status-controller';
import checkToken from '../middlewares/checkToken';
import getUser from '../middlewares/getUser';

const router = express.Router();

router.get('/statuses', PagesStatusController.getAll);
router.post('/statuses', checkToken, getUser, PagesStatusController.create);
router.put('/statuses', checkToken, getUser, PagesStatusController.update);
router.delete('/statuses', checkToken, getUser, PagesStatusController.deleteStatus);

export default router;