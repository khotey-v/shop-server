import Page from '../models/page';
import Status from '../models/status';

export default async function (pageData) {
  const errors = [];
  const {
    title,
    url,
    body,
    status,
    seo,
    _id
  } = pageData;
  const searchQuery = { url };

  if (_id) {
    searchQuery._id = { $ne: _id };
  }

  if (!title) {
    errors.push('Поле title обязательно');
  }

  if (!url) {
    errors.push('Поле url обязательно');
  }

  if (!body) {
    errors.push('Поле body обязательно');
  }

  if (!status) {
    errors.push('Поле status обязательно');
  }

  if (!seo || !(seo.description && seo.keywords)) {
    errors.push('Поля seo обязательны');
  }

  let page;

  try {
    page = await Page.findOne(searchQuery);
  } catch(e) {
    throw e;
  }

  if (page) {
    errors.push(`Страница с ссылкой ${url} уже существует`);
  }

  let _status;

  try {
    _status = await Status.findOne({ _id: status });
  } catch(e) {
    throw e;
  }

  if (!_status) {
    errors.push(`Статуса ${status} не существует`);
  }

  return errors;
}