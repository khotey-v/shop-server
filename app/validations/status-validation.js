import * as StatusService from '../services/status-service';

export default async function(statusData) {
  // check is name empty
  const { name, statusType } = statusData;
  const errors = [];

  if (!name) {
    errors.push('Название статуса обьязательно');
  }

  if (!statusType) {
    errors.push('Не указан тип статуса');
  }

  // TODO: add check for update like in pages
  // check is status unique
  let status = await StatusService.getStatusByName(name, statusType);

  if (status) {
    errors.push(`Статус с названием '${name}' уже существует`);
  }

  return errors;
}