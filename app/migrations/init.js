import mongoose from 'mongoose';
import bluebird from 'bluebird';
import fs from 'fs';

import User from '../models/user';
// import PageStatus from '../models/page-status';
// import OrderStatus from '../models/order-status';

import config from '../config';

const pageStatuses = [
  { name: 'enable' },
  { name: 'disable' },
];

const orderStatuses = [
  { name: 'new' },
  { name: 'confirmed' },
  { name: 'cancelled' },
  { name: 'done' },
];

const adminData = {
  login: 'admin',
  password: 1111
};

mongoose.Promise = bluebird;

mongoose.connect(config.database, err => {
  if (err) throw err;

  init();
});

async function init() {
  let file = fs.openSync('./custom-data.txt', 'w');

  await User.remove({});
  // await PageStatus.remove({});
  // await OrderStatus.remove({});

  console.log('Clearing data \n');
  console.log('Admin account:');

  fs.writeSync(file, `Admin account\n`);

  try {
    let admin = await createUser(adminData);

    console.log(admin);
    fs.writeSync(file, `${admin.toString()}\n`);
  } catch ({ message }) {
  }

  console.log('\n');
  // console.log('Page Statuses:');

  // fs.appendFileSync(file, `\n`);
  // fs.appendFileSync(file, `Page Statuses:\n`);

  // for (let pageStatus of pageStatuses) {
  //   let status;

  //   try {
  //     status = await createPageStatus(pageStatus);

  //     console.log(status);
  //     fs.appendFileSync(file, `${status.toString()}\n`);
  //   } catch ({ message }) {
  //     console.error(message);
  //   }
  // }

  // fs.appendFileSync(file, `\n`);

  // console.log('\n');
  // console.log('Order Statuses:');

  // fs.appendFileSync(file, `Order Statuses:\n`);

  // for (let orderStatus of orderStatuses) {
  //   let status;

  //   try {
  //     status = await createOrderStatus(orderStatus);

  //     console.log(status);
  //     fs.appendFileSync(file, `${status.toString()}\n`);
  //   } catch ({ message }) {
  //     console.error(message);
  //   }
  // }

  // console.log('-------------------------');

  fs.closeSync(file);
  process.exit();
}

function createUser(userData) {
  return User.create(userData);
}

function createPageStatus(pageStatusData) {
  return PageStatus.create(pageStatusData);
}

function createOrderStatus(orderStatusData) {
  return OrderStatus.create(orderStatusData);
}


