import Status from '../models/status';
import statusValidate from '../validations/status-validation';

export async function getAll(req, res, next) {
  let statuses;

  try {
    statuses = await Status.find({ statusType: 1 });
  } catch({ message }) {
    return next({
      message
    });
  }

  res.json({ statuses });
}

export async function create(req, res, next) {
  const statusData = req.body;
  let status = new Status(statusData);
  let errors;

  try {
    errors = await statusValidate(statusData);
  } catch({ message }) {
    return next({
      message
    });
  }

  if (errors.length) {
    return next({
      status: 400,
      message: errors
    });
  }

  try {
    await status.save();
  } catch({ message }) {
    return next({
      message
    });
  }

  res.json(status);
}

export async function update(req, res, next) {
  const statusData = req.body;
  const { _id } = statusData;
  let errors;
  let status;

  if (!_id) {
    return next({
      status: 400,
      message: 'Не указан id для обновления статуса'
    });
  }

  try {
    status = await Status.findOne({ _id });
  } catch({ message }) {
    return next({
      message
    })
  }

  if (!status) {
    return next({
      status: 404,
      message: 'Статус не найден'
    });
  }

  try {
    errors = await statusValidate(statusData);
  } catch({ message }) {
    return next({
      message
    });
  }

  if (errors.length) {
    return next({
      status: 400,
      message: errors
    });
  }

  try {
    status = await Status.update({ _id }, statusData);
  } catch({ message }) {
    return next({
      message
    })
  }

  res.json(statusData);
}

export async function deleteStatus(req, res, next) {
  const { _id } = req.body;

  if (!_id) {
    return next({
      status: 400,
      message: 'Не указан id для обновления статуса'
    });
  }
  let status;

  try {
    status = await Status.findOne({ _id });
  } catch({ message }) {
    return next({
      message
    });
  }
  
  if (!status) {
    return next({
      status: 404,
      message: 'Статус не найден'
    });
  }

  status.remove(err => {
    res.json({ message: 'Статус удален'});
  });
}