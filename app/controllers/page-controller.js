import Page from '../models/page';
import pageValidate from '../validations/page-validation';
import mongoose from 'mongoose';

export async function getAll(req, res, next) {
  let pages;

  try {
    pages = await Page.find({});
  } catch({ message }) {
    return next({
      message
    });
  }

  res.json({ pages });
}

export async function getByUrl(req, res, next) {
  const { url } = req.params;
  let page;

  try {
    page = await Page.findOne({ url });
  } catch({ message }) {
    return next({
      message
    });
  }

  if (!page) {
    return next({
      status: 404,
      message: `Страницы с ссылкой ${url}`
    })
  }

  res.json(page);
}

export async function create(req, res, next) {
  const pageData = req.body;
  let errors;
  let page;

  try {
    errors = await pageValidate(pageData);
  } catch({ message }) {
    return next({
      message
    });
  }

  if (errors.length) {
    return next({
      status: 400,
      message: errors
    });
  }

  try {
    page = await Page.create(pageData);
  } catch({ message }) {
    return next({
      message
    });
  }

  res.json(page);
}

export async function update(req, res, next) {
  const pageData = req.body;
  let errors;
  let page;

  try {
    errors = await pageValidate(pageData);
  } catch({ message }) {
    return next({
      message
    });
  }

  if (errors.length) {
    return next({
      status: 400,
      message: errors
    });
  }

  try {
    page = await Page.update({ _id: pageData._id }, pageData);
  } catch({ message }) {
    return next({
      message
    });
  }

  res.json(pageData);
}

export async function deletePage(req, res, next) {
  const { _id } = req.body;
  let page;

  if (!_id) {
    return next({
      status: 400,
      message: 'Не указан _id страницы'
    });
  }

  const isValidId = mongoose.Types.ObjectId.isValid(_id);

  if (!isValidId) {
    return next({
      status: 400,
      message: `Не валидный _id записи ${_id}`
    });
  }


  try {
    page = await Page.findOne({ _id });
  } catch({ message }) {
    return next({
      message
    });
  }

  if (!page) {
    return next({
      status: 400,
      message: `Страница с ${_id} не найдена`
    });
  }

  page.remove(err => {
    if (err) {
      return next({
        message: err.message
      });
    }

    res.json({ message: `Запись с _id: ${_id} удалена` });
  })
}