import jwt from 'jsonwebtoken';

import User from '../models/user';
import config from '../config';

export async function signin(req, res, next) {
  if (!req.body || !req.body.login || !req.body.password) {
    return next({
      status: 400,
      message: 'Заполните форму'
    });
  }

  let { login, password } = req.body;
  let user;

  password = password.toString();

  try {
    user = await User.findOne({ login });
  } catch({ message }) {
    return next({
      status: 400,
      message
    });
  }

  if (!user) {
    return next({
      status: 404,
      message: 'Пользователь не найден'
    });
  }

  let result;

  try {
    result = await user.comparePasswords(password);
  } catch({ message }) {
    return next({
      status: 500,
      message
    });
  }

  if (!result) {
    return next({
      status: 400,
      message: 'Данные неверны'
    });
  }

  const token = jwt.sign({ _id: user._id }, config.secret);

  res.json({ token });
}