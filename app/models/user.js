import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcrypt';

const UserSchema = new Schema({
  login: { 
    type: String, 
    lowercase: true, 
    unique: [true, 'Пользователь с таким логин существует'], 
    require: [true, 'Введите поле логин'] 
  },
  password: String
});

UserSchema.pre('save', async function(next) {
  if (!this.isModified('password')) {
    return next();
  }

  let salt;
  let hash;

  try {
    salt = await bcrypt.genSalt(10);
    hash = await bcrypt.hash(this.password, salt);
  } catch(e) {
    throw e;
  }

  this.password = hash;

  next();
});

UserSchema.methods.comparePasswords = async function(password) {
  return await bcrypt.compare(password, this.password);
};

export default mongoose.model('Users', UserSchema);
