import mongoose, { Schema } from 'mongoose';

const StatusSchema = new Schema({
  name: { 
    type: String, 
    require: [true, 'Название статуса не заполнено'], 
    unique: [true, 'Статус с таким названием уже существует'],
  },
  statusType: { 
    type: Number,
    enum: [1, 2], // 1 - for pages, 2 - for orders
    require: [true, 'Тип статуса не указан'], 
  }
});

export default mongoose.model('Statuses', StatusSchema);
