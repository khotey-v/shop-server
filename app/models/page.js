import mongoose, { Schema } from 'mongoose';

const PageSchema = new Schema({
  title: { 
    type: String,
    require: [true, 'Поле титул не заполнено']
  },
  url: {
    type: String,
    require: [true, 'Полу ссыка не заполенено'],
    unique: [true, 'Старица с такой ссылкой уже существует']
  },
  body: { 
    type: String, 
    require: [true, 'Тело страницы не заполнено']
  },
  seo: {
    description: { 
      type: String, 
      require: [true, 'СЕО описание страницы не заполнено']
    },
    keywords: { 
      type: String, 
      require: [true, 'СЕО ключи сраницы не заполнены']
    },
  },
  status: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'PagesStatuses',
    require: [true, 'Не выбрат статус страницы']
  },
  user: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Users',
  }
});

export default mongoose.model('Pages', PageSchema);