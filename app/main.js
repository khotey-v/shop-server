import app from '../app';

import errorHandler from './middlewares/errorHandler';
import authRoutes from './routes/auth-routes';
import statusRoutes from './routes/status-routes';
import pageRoutes from './routes/page-routes';

app.use('/api', authRoutes);
app.use('/api', statusRoutes);
app.use('/api', pageRoutes);

app.use(errorHandler);

export default app;
