import Status from '../models/status';

/**
 * @param  {string} status name
 * @param  {type} status type
 * @return {Status}
 */
export async function getStatusByName(name, type) {
  let status = await Status.findOne({ name, statusType: type });

  return status;
}