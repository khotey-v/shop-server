import 'babel-polyfill';
import chai from 'chai';
import chaiHttp from 'chai-http';
import async from 'async';

import app from '../../main';

import Status from '../../models/status';
import User from '../../models/user';

let should = chai.should();

chai.use(chaiHttp);

describe('Statuses', () => {
  afterEach(done => {
    Status.remove({}, err => {
      if (err) throw err;

      done();
    });
  })

  describe('/GET /api/statuses', () => {

    it('it should return array of statuses', done => {
      const statusData = {
        name: 'test-status',
        statusType: 1,
      };

      const status = new Status(statusData);

      status.save((err, newStatus) => {
        if (err) throw err;

        const statusesArray = [];

        statusesArray.push(newStatus);

        chai.request(app)
          .get('/api/statuses')
          .end((err, res) => {
            res.status.should.equal(200);
            res.body.should.have
              .property('statuses');

            let statusFromReq = res.body.statuses[0];

            // statusFromReq.should.have.property('_id').to.deep.equal(newStatus._id);
            statusFromReq.should.have.property('name').to.deep.equal(newStatus.name);
            statusFromReq.should.have.property('statusType').to.deep.equal(newStatus.statusType);

            done();
          });
      });
    });
  });

  describe('/POST /api/statuses', () => {
    let token;

    before(done => {
      const userData = {
        login: 'test-login',
        password: 1111
      };

      async.waterfall([
        function createUser(callback) {
          User.create(userData, (err, user) => {
            callback(null, user);
          });
        },

        function loginUser(user, callback) {
          chai.request(app)
            .post('/api/signin')
            .send(userData)
            .end((err, res) => {
              token = res.body.token;

              callback(null);
              done();
            });
        }
      ]);
    });

    it('it should return 403 error, message no token', done => {
      chai.request(app)
        .post('/api/statuses')
        .end((err, res) => {
          res.status.should.equal(403);
          res.body.should.have
            .property('message')
            .equal('Forbidden. No Token');

          done();
        });
    });

    it('it should return 400 error, message invalid token', done => {
      chai.request(app)
        .post('/api/statuses')
        .set('Authorization', 'invalidtokenvalue')
        .end((err, res) => {
          res.status.should.equal(400);
          res.body.should.have
            .property('message')
            .equal('jwt malformed');

          done();
        });
    });

    it('it should create status and return this status', done => {
      const statusData = {
        name: 'test-status',
        statusType: 1,
      };

      chai.request(app)
        .post('/api/statuses')
        .set('Authorization', token)
        .send(statusData)
        .end((err, res) => {
          res.status.should.equal(200);
          res.body.should.have.property('_id');
          res.body.should.have.property('name').equal(statusData.name);
          res.body.should.have.property('statusType').equal(statusData.statusType);

          done();
        });
    });

    it('it should return 400 on duplicate status', done => {
      const statusData = {
        name: 'test-status',
        statusType: 1,
      };

      const statusData2 = {
        name: 'test-status',
        statusType: 1,
      };

      const status2 = new Status(statusData2);

      status2.save((err, newStatus2) => {
        if (err) throw err;

        chai.request(app)
          .post('/api/statuses')
          .set('Authorization', token)
          .send(statusData)
          .end((err, res) => {
            res.status.should.equal(400);
            res.body.should.have.property('message');
            res.body.message[0].should.equal(`Статус с названием '${statusData.name}' уже существует`);

            done();
          });
      });
    });

  });

  describe('/PUT /api/statuses', () => {
    let token;

    before(done => {
      const userData = {
        login: 'test-login',
        password: 1111
      };

      async.waterfall([
        function createUser(callback) {
          User.create(userData, (err, user) => {
            callback(null, user);
          });
        },

        function loginUser(user, callback) {
          chai.request(app)
            .post('/api/signin')
            .send(userData)
            .end((err, res) => {
              token = res.body.token;

              callback(null);
              done();
            });
        }
      ]);
    });

    it('it should update status', done => {
      const statusData = {
        name: 'test-status',
        statusType: 1,
      };

      const status = new Status(statusData);

      status.save((err, newStatus) => {
        if (err) throw err;

        const statusDataForUpdate = {};

        statusDataForUpdate._id = newStatus._id;
        statusDataForUpdate.name = 'updated-status';
        statusDataForUpdate.statusType = 1;

        chai.request(app)
          .put('/api/statuses')
          .set('Authorization', token)
          .send(statusDataForUpdate)
          .end((err, res) => {
            res.status.should.equal(200);
            res.body.should.have
              .property('_id');
            res.body.should.have
              .property('name')
              .equal(statusDataForUpdate.name);
            res.body.should.have
              .property('statusType')
              .equal(statusDataForUpdate.statusType);

            done();
          });
      });
    });

    it('it should return error 400 no id for update', done => {
      const statusData = {
        name: 'test-status',
        statusType: 1,
      };

      const status = new Status(statusData);

      status.save((err, newStatus) => {
        if (err) throw err;

        const statusDataForUpdate = {};

        statusDataForUpdate.name = 'updated-status';
        statusDataForUpdate.statusType = 1;

        chai.request(app)
          .put('/api/statuses')
          .set('Authorization', token)
          .send(statusDataForUpdate)
          .end((err, res) => {
            res.status.should.equal(400);
            res.body.should.have
              .property('message')
              .equal('Не указан id для обновления статуса');

            done();
          });
      });
    });

    it('it should return error 400 no field', done => {
      const statusData = {
        name: 'test-status',
        statusType: 1,
      };

      const status = new Status(statusData);

      status.save((err, newStatus) => {
        if (err) throw err;

        const statusDataForUpdate = {};

        statusDataForUpdate.name = 'updated-status';
        statusDataForUpdate._id = newStatus._id;

        chai.request(app)
          .put('/api/statuses')
          .set('Authorization', token)
          .send(statusDataForUpdate)
          .end((err, res) => {
            res.status.should.equal(400);
            res.body.should.have
              .property('message');

            res.body.message[0].should.equal('Не указан тип статуса');

            done();
          });
      });
    });

    it('it should return error 500 for invalid id', done => {
      const statusData = {
        name: 'test-status',
        statusType: 1,
      };

      const status = new Status(statusData);

      status.save((err, newStatus) => {
        if (err) throw err;

        const statusDataForUpdate = {};

        statusDataForUpdate.name = 'updated-status';
        statusDataForUpdate._id = 15;

        chai.request(app)
          .put('/api/statuses')
          .set('Authorization', token)
          .send(statusDataForUpdate)
          .end((err, res) => {
            res.status.should.equal(500);

            done();
          });
      });
    });

  });

  describe('/DELETE /api/statuses', () => {
    let token;

    before(done => {
      const userData = {
        login: 'test-login',
        password: 1111
      };

      async.waterfall([
        function createUser(callback) {
          User.create(userData, (err, user) => {
            callback(null, user);
          });
        },

        function loginUser(user, callback) {
          chai.request(app)
            .post('/api/signin')
            .send(userData)
            .end((err, res) => {
              token = res.body.token;

              callback(null);
              done();
            });
        }
      ]);
    });

    it('it should delete status', done => {
      const statusData = {
        name: 'test-status',
        statusType: 1,
      };

      const status = new Status(statusData);

      status.save((err, newStatus) => {
        if (err) throw err;

        const statusDataForUpdate = {};

        statusDataForUpdate.name = 'updated-status';
        statusDataForUpdate.statusType = 1;

        chai.request(app)
          .delete('/api/statuses')
          .set('Authorization', token)
          .send({
            _id: newStatus._id
          })
          .end((err, res) => {
            res.status.should.equal(200);
            res.body.should.have
              .property('message')
              .equal('Статус удален');

            done();
          });
      });
    });

    it('it should return error 400 no id for update', done => {
      const statusData = {
        name: 'test-status',
        statusType: 1,
      };

      const status = new Status(statusData);

      status.save((err, newStatus) => {
        if (err) throw err;

        const statusDataForUpdate = {};

        statusDataForUpdate.name = 'updated-status';
        statusDataForUpdate.statusType = 1;

        chai.request(app)
          .delete('/api/statuses')
          .set('Authorization', token)
          .send({})
          .end((err, res) => {
            res.status.should.equal(400);
            res.body.should.have
              .property('message')
              .equal('Не указан id для обновления статуса');

            done();
          });
      });
    });

  });
});