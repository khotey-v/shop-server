import 'babel-polyfill';
import chai from 'chai';
import chaiHttp from 'chai-http';
import async from 'async';

import app from '../../main';
import Page from '../../models/page';
import Status from '../../models/status';
import User from '../../models/user';

let should = chai.should();

chai.use(chaiHttp);

describe('Pages', function() {
  let statusEnable;
  let statusDisable;
  let token;

  beforeEach(done => {
    const statusEnableData = { name: 'enable' };
    const statusDisableData = { name: 'disable' };
    const userData = {
      login: 'test-login',
      password: 1111
    };

    async.waterfall([
      function createStatusEnable(callback) {
        Status.create(statusEnableData, (err, status) => {
          statusEnable = status;

          callback(null);
        });
      },

      function createStatusDisable(callback) {
        Status.create(statusDisableData, (err, status) => {
          statusDisable = status;

          callback(null);
        });
      },

      function createUser(callback) {
        User.create(userData, (err, user) => {
          callback(null, user);
        });
      },

      function loginUser(user, callback) {
        chai.request(app)
          .post('/api/signin')
          .send(userData)
          .end((err, res) => {
            token = res.body.token;

            callback(null);
            done();
          });
      }

    ]);
  });

  afterEach(done => {
    async.waterfall([
      function removeStatuses(callback) {
        Status.remove({}, err => {
          callback(null);
        });
      },

      function removePages(callback) {
        Page.remove({}, err => {

          done();
          callback(null);
        });
      }
    ]);
  });

  describe('/GET /api/pages', function() {

    it('it should return array of pages', function(done) {
      const pageData = {
        status: statusEnable._id,
        title: 'page title',
        url: 'page-title',
        body: 'page body',
        seo: {
          description: 'description',
          keywords: 'keywords'
        }
      };

      Page.create(pageData, (err, page) => {
        if (err) throw err;

        chai.request(app)
          .get('/api/pages')
          .end((err, res) => {
            res.status.should.equal(200);
            res.body.should.have
            .property('pages')
            .to.be.a('array');

            const pageFromRequest = res.body.pages[0];

            pageFromRequest.title.should.equal(page.title);
            pageFromRequest.url.should.equal(page.url);
            pageFromRequest.body.should.equal(page.body);
            done();
          })
      });
    });

    it('it should return page by id', function(done) {
      const pageData = {
        status: statusEnable._id,
        title: 'page title',
        url: 'page-title',
        body: 'page body',
        seo: {
          description: 'description',
          keywords: 'keywords'
        }
      };

      Page.create(pageData, (err, page) => {
        if (err) throw err;

        chai.request(app)
          .get(`/api/pages/${page.url}`)
          .end((err, res) => {
            res.status.should.equal(200);

            const pageFromRequest = res.body;
            pageFromRequest.should.to.be.a('object');

            pageFromRequest.title.should.equal(page.title);
            pageFromRequest.url.should.equal(page.url);
            pageFromRequest.body.should.equal(page.body);

            done();
          });
      });
    });

    it('it should return error 404 for page by id not found', function(done) {
      const pageData = {
        status: statusEnable._id,
        title: 'page title',
        url: 'page-title',
        body: 'page body',
        seo: {
          description: 'description',
          keywords: 'keywords'
        }
      };

      Page.create(pageData, (err, page) => {
        if (err) throw err;

        chai.request(app)
          .get(`/api/pages/${page.url}-notfound`)
          .end((err, res) => {
            res.status.should.equal(404);
            res.body.should.have
              .property('message')
              .equal(`Страницы с ссылкой ${page.url}-notfound`);

            done();
          });
      });
    });

  });

  describe('/POST /api/pages', function() {
    it('it should return page created data', done => {
      const pageData = {
        status: statusEnable._id,
        title: 'page title',
        url: 'page-title',
        body: 'page body',
        seo: {
          description: 'description',
          keywords: 'keywords'
        }
      };

      chai.request(app)
        .post('/api/pages')
        .set('Authorization', token)
        .send(pageData)
        .end((err, res) => {
          res.status.should.equal(200);

          const page = res.body;

          page.should.to.be.a('object');
          page.should.have.property('_id');
          page.should.have.property('title').equal(pageData.title);
          page.should.have.property('url').equal(pageData.url);
          page.should.have.property('status').equal(statusEnable._id.toString());

          done();
        });

    });

    it('it should return error 400 with no validation', done => {
      const pageData = {
        status: statusEnable._id,
        title: 'page title',
        url: 'page-title',
        body: 'page body',
        seo: {
          description: 'description',
          keywords: 'keywords'
        }
      };

      Page.create(pageData, (err, page) => {
        if (err) throw err;

        chai.request(app)
          .post('/api/pages')
          .set('Authorization', token)
          .send(pageData)
          .end((err, res) => {
            res.status.should.equal(400);
            res.body.should.to.be.a('object');
            res.body.should.have
              .property('message')
              .to.be.a('array');

            const message = res.body.message[0];

            message.should.equal(`Страница с ссылкой ${pageData.url} уже существует`)

            done();
          });
      });

    });

  });

  describe('/PUT /api/pages', function() {
    it('it should update page and return page', done => {
      const pageData = {
        status: statusEnable._id,
        title: 'page title',
        url: 'page-title',
        body: 'page body',
        seo: {
          description: 'description',
          keywords: 'keywords'
        }
      };

      Page.create(pageData, (err, page) => {
        if (err) throw err;

        page.title = 'new-title';

        chai.request(app)
          .put('/api/pages')
          .set('Authorization', token)
          .send(page)
          .end((err, res) => {
            res.status.should.equal(200);
            res.body.should.to.be.a('object');
            res.body.should.have
              .property('_id')
              .equal(page._id.toString());

            res.body.should.have
              .property('title')
              .equal('new-title');

            done();
          });
      });
    });

    it('it should return error 400 for dup url', done => {
      const pageData = {
        status: statusEnable._id,
        title: 'page title',
        url: 'page-title',
        body: 'page body',
        seo: {
          description: 'description',
          keywords: 'keywords'
        }
      };

      const pageDataForUpdate = {
        status: statusEnable._id,
        title: 'page title',
        url: 'page-title2',
        body: 'page body',
        seo: {
          description: 'description',
          keywords: 'keywords'
        }
      };

      async.waterfall([
        function createFirstPage(callback) {
          Page.create(pageData, (err, page) => {
            callback(null);
          });
        },

        function createPageForUpdate(callback) {
          Page.create(pageDataForUpdate, (err, page) => {
            callback(null);
          });
        },
      ], function() {

        pageDataForUpdate.url = pageData.url;

        chai.request(app)
          .put('/api/pages')
          .set('Authorization', token)
          .send(pageDataForUpdate)
          .end((err, res) => {
            res.status.should.equal(400);
            res.body.should.to.be.a('object');
            res.body.should.have
              .property('message')
              .to.be.a('array');

            const message = res.body.message[0];

            message.should.equal(`Страница с ссылкой ${pageDataForUpdate.url} уже существует`)

            done();
          });
      });
    });

  });

  describe('/DELETE /api/pages', function() {
    it('it should retrun error 400: no _id', done => {
      chai.request(app)
        .delete('/api/pages')
        .set('Authorization', token)
        .end((err, res) => {
          res.status.should.equal(400);
          res.body.should.have
            .property('message')
            .equal('Не указан _id страницы');

          done();
        });
    });

    it('it should retrun error 400: page with _id not found', done => {
      const _id = 'adasdasdasd';

      chai.request(app)
        .delete('/api/pages')
        .set('Authorization', token)
        .send({ _id })
        .end((err, res) => {
          res.status.should.equal(400);
          res.body.should.have
            .property('message')
            .equal(`Не валидный _id записи ${_id}`);

          done();
        });
    });

    it('it should retrun success delete', done => {
      const pageData = {
        status: statusEnable._id,
        title: 'page title',
        url: 'page-title',
        body: 'page body',
        seo: {
          description: 'description',
          keywords: 'keywords'
        }
      };

      Page.create(pageData, (err, page) => {
        if (err) throw err;

        chai.request(app)
          .delete('/api/pages')
          .set('Authorization', token)
          .send({ _id: page._id })
          .end((err, res) => {
            res.status.should.equal(200);
            res.body.should.have
              .property('message')
              .equal(`Запись с _id: ${page._id} удалена`);

            done();
          });
      });
    });

  });

});