import 'babel-polyfill';
import chai from 'chai';
import chaiHttp from 'chai-http';

import app from '../../main';
import User from '../../models/user';

let should = chai.should();

chai.use(chaiHttp);

describe('Authorization', () => {
  after(done => {
    User.remove({}, function(err) {
      if (err) throw err;

      done();
    });
  });

  describe('/POST /api/signin', () => {
    it('it should return Error 400: Заполните форму, for no login, password in request', done => {
      chai.request(app)
        .post('/api/signin')
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.have
            .property('message')
            .eql('Заполните форму');

            done();
        });
    });

    it('it should return Error 404: Пользователь не найден', done => {
      chai.request(app)
        .post('/api/signin')
        .send({ login: 'no-user', password: 1 })
        .end((err, res) => {
          res.should.have.status(404);
          res.body.should.have
            .property('message')
            .eql('Пользователь не найден');

            done();
        });
    });

    it('it should return Error 400: Неверные данные, for incorrect password', done => {
      const userData = { login: 'vitaliy', password: 1111 };
      const user = new User(userData);

      user.save((err, user) => {
        chai.request(app)
          .post('/api/signin')
          .send({ login: userData.login , password: 1 })
          .end((err, res) => {
            res.should.have.status(400);
            res.body.should.have
              .property('message')
              .eql('Данные неверны');

              done();
          });
      });
    });

    it('it should return token: for valid user data', done => {
      const userData = { login: 'vitaliy', password: 1111 };
      const user = new User(userData);

      user.save((err, user) => {
        chai.request(app)
          .post('/api/signin')
          .send(userData)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have
              .property('token');

              done();
          });
      });
    });
  });
})